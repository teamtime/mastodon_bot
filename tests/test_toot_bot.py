"""Tests for the toot_bot module."""

# pylint: disable=missing-docstring

import toot_bot

def test_default_include_article():
    assert toot_bot.default_include_article('some article')
    assert not toot_bot.default_include_article(None)
