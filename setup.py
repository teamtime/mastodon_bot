"""mastodon_bot installer."""

from setuptools import setup

setup(
    name='mastodon_bot',
    version='0.1.0',
    description='An announcement bot for Mastodon instances',
    url='https://gitlab.com/photog.social/mastodon_bot',
    author='KemoNine',
    author_email='mcrosson_mastobot@kemonine.info',
    license='GPLv3',
    setup_requires=['pytest-runner'],
    tests_require=['pytest'],
)
