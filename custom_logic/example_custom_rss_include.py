"""Example of how to build an article filter."""

def include_article(article):
    """Custom include logic for dPS photog challenges posted on main RSS feed."""
    if 'Weekly' in article.title and 'Challenge' in article.title:
        return True

    return False
