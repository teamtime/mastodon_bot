# General info
Welcome to the Mastodon Bot project page. This project aims to provide a Mastodon bot that toots reminds and/or cross posts articles from an RSS feed. It's most useful for those "Don't forget we have a blog/patreon/etc" posts and/or cross posting from an announcements blog to an instance.

This was designed with the admin in mind and functionality will grow over time.

# Features
- Ability to regularly post a toot + sub toots
- Ability to toot articles that are in an RSS feed
- Ability to hide toots behind CW text

# Compatibility
- Mastodon 1.4.x

# Requirements
- Python 3.x
- Mastodon.py Python module
- feedparser Python Module
- ruamel.yaml Python module
- argparse Python module (should be built in)
- sqlite3 Python module (should be built in)

# Installation
1. Install the dependencies: ```pip3 install -r requirements.txt```
1. Install the bot: ```python setup.py install```
1. Run the bot: ```python .\toot_bot.py --config .\configs\config.yaml [init|login|toot|rss]```

# Testing
1. For development, install the bot with: ```python setup.py develop```
1. Use [pytest](https://docs.pytest.org/en/latest/) to run tests: ```python setup.py develop```

# Tooting
## Reminder Toots
To send reminder toots take a look at 'configs/example_reminder.yaml' and do the following
1. Run python ```.\toot_bot.py --config .\configs\example_reminder.yaml init```
1. Run python ```.\toot_bot.py --config .\configs\example_reminder.yaml toot```

The above will initialize the configuration (including login, you'll be prompted for user/password) and then toot what has been setup in your config file.

You'll probably want to run the 2nd command on a schedule via cron for auto-tooting on a schedule

## RSS Toots
To cross post RSS articles take a look at 'configs/example_rss.yaml' and do the following
1. Run python ```.\toot_bot.py --config .\configs\example_rss.yaml init```
1. Run python ```.\toot_bot.py --config .\configs\example_rss.yaml rss```

The above will initialize the configuration (including login, you'll be prompted for user/password) then toot the various articles from the RSS feed. *BE CAREFUL*, the cache will be empty to start and you might spam your instance if there are a large number of articles in the feed.

Note: the RSS cache will be initialized the first time you run the rss command

You'll probably want to run the 2nd command on a schedule via cron for cross posting on a schedule

You can also implement custom logic for filtering RSS feeds, take a look at ```configs/example_rss_custom_logic.yaml``` and ```custom_logic/example_custom_rss_include.py``` for ideas.

## Welcome Bot
To welcome new users the first time they toot take a look at 'configs/example_welcome_config.yaml' and do the following
1. Run python ```.\toot_bot.py --config .\configs\example_welcome_config.yaml init```
1. Run python ```.\toot_bot.py --config .\configs\example_welcome_config.yaml welcome```

The above will initialize the configuration (including login, you'll be prompted for user/password) then toot a welcome message to any new users that toot and haven't been seen by the bot. It's a great way to say hello to new users of an instance. *BE CAREFUL*, the cache will be empty to start and you might spam your instance if there are a large number of existing toots.

Seriously, this is DANGEROUS if your instance has been around for a bit. You may want to try it in development first and/or create the sqlite database by hand and insert a row into toot_cache close to the current toot id of your instance.

Note: the welcome cache will be initialized the first time you run the welcome command

You'll probably want to run the 2nd command via the sample systemd unit file at ```configs/example_welcome_bot_service```.

## TOTP
If you're using a bot with an account that has 2FA / TOTP enabled you can pass the ```--totp``` option to ```init``` and ```login``` to make the init and login functions work correctly. There will be additional steps and you'll be prompted to complete the necessary steps.

# Contributing
Contributions to this project are welcome and appreciated. Please create a pull request with any changes you'd like to see included. The developers(s) will review the pull request and make any necessary comments/approvals after review.

Please note all code and documentation (except libraries as appropriate) must be licensed according to the "Licensing" section of the README.

# Licensing
## Code
All code in this project is licensed under the GPLv3. The "GPLv3" file contains the full license.
## Documentation
All documentation is licensed under the Creative Commons Attribution, Non Commercial, Share Alike 4.0 International license (CC BY-NC-SA 4.0). The "CC_BY-NC-SA_4.0.html" file contains the full license.

![Creative Commons License](https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png)
